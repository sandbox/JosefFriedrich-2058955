<?php

/**
 * @file
 * Page callbacks.
 */

/**
 * Page callback: Shows all blocks of the module for debuggin purposes.
 *
 * @see pgn_block_menu()
 */
function pgn_block_show_blocks() {
  $build = array();

  $blocks = pgn_block_block_info();
  foreach ($blocks as $delta => $block) {
    $build[$delta]['heading']['#markup'] = '<hr><h2>' . $block['info'] . '</h2>';
    $build[$delta]['block'] = pgn_block_get_block($delta);
  }

  return $build;
}

/**
 * Return a block as a render array.
 *
 * @param string $delta
 *   Which block is being configured. This is a unique identifier for the block
 *   within the module, defined in hook_block_info().
 *
 * @return array
 *   A render array.
 */
function pgn_block_get_block($delta) {
  $block = block_load('pgn_block', $delta);
  $block_render_blocks = _block_render_blocks(array($block));
  return _block_get_renderable_array($block_render_blocks);
}
