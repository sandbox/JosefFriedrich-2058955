<?php

/**
 * @file
 * Theme functions.
 */

/**
 * Returns a text wrapped into HTML tags.
 *
 * @param array $variables
 *   An associative array containing:
 *   - tag
 *   - text
 *
 * @ingroup themeable
 */
function theme_pgn_block_text($variables) {
  $tag = $variables['tag'];
  $text = $variables['text'];

  if (!$tag) {
    return $text;
  }
  else {
    return '<' . $tag . '>' . $text . '</' . $tag . '>';
  }
}

/**
 * Returns a rendered image, which lies in the "images" image folder.
 *
 * @param array $variables
 *   An associative array containing:
 *   - image: Name of the image file which lies in the "images" folder.
 *
 * @ingroup themeable
 */
function theme_pgn_block_image($variables) {
  $image = $variables['image'];

  $path = url(drupal_get_path('module', 'pgn_block')) . '/images/' . $image;
  return theme('image', array('path' => $path));
}

/**
 * Returns a link either as a linked image or a linked text.
 *
 * @param array $variables
 *   An associative array containing:
 *   - html: Boolean: "TRUE" returns inline image. "FALSE" returns text link.
 *   - image: Name of the image file which lies in the "images" folder.
 *   - path: Path of the link.
 *   - tag: Tag name, e. g. "p", "h2".
 *   - text: Text to show as link text.
 *   - title: Text shown in the title attribute of the link.
 *   - token: Token to autoload a image and show css class and id.
 *
 * @ingroup themeable
 */
function theme_pgn_block_link($variables) {
  $html = $variables['html'];
  $image = $variables['image'];
  $path = $variables['path'];
  $tag = $variables['tag'];
  $text = $variables['text'];
  $title = $variables['title'];
  $token = $variables['token'];

  if (!$image) {
    $image = $token . '.svg';
  }

  if ($html) {
    $text = theme('pgn_block_image', array('image' => $image));
    $css_class = 'img-html';
    $css_id = '';
  }
  else {
    $css_class = 'css-background';
    $css_id = $token;
  }

  $options = array(
    'html' => $html,
    'attributes' => array(
      'title' => $title,
      'class' => array($css_class),
      'id' => array($css_id),
    ),
  );

  $output = l($text, $path, $options);

  if ($tag) {
    return theme('pgn_block_text', array('text' => $output, 'tag' => $tag));
  }
  else {
    return $output;
  }

}
