<?php

/**
 * @file
 * Creates a panels pane.
 */

$plugin = array(
  'single' => FALSE,
  'title' => 'Titel und Slogan',
  'description' => 'Pane für Titel- und Slogan-Texte in großer Schrift.',
  'category' => 'PGN',
  'edit form' => 'pgn_block_custom_pane_edit_form',
  'render callback' => 'pgn_block_custom_pane_render',
  'admin info' => 'pgn_block_custom_pane_admin_info',
  'defaults' => array(
    'text' => '',
    'tag' => '',
  ),
  'all contexts' => TRUE,
);

/**
 * Callback for 'edit form'.
 */
function pgn_block_custom_pane_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $form['text'] = array(
    '#type' => 'select',
    '#title' => 'Textbausteine',
    '#options' => pgn_block_options_text(),
    '#description' => 'Wählen Sie die Textbausteine aus, die angezeigt werden sollen.',
    '#default_value' => !empty($conf['text']) ? $conf['text'] : 'title',
  );

  $form['tag'] = array(
    '#type' => 'select',
    '#title' => 'HTMl-Tag',
    '#options' => pgn_block_options_tag(),
    '#description' => 'Wählen Sie den HTML-Tag aus, der den Text umrahmen soll.',
    '#default_value' => 'h2',
  );

  return $form;
}

/**
 * Submit function for 'edit form' callback.
 */
function pgn_block_custom_pane_edit_form_submit(&$form, &$form_state) {
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    if (isset($form_state['values'][$key])) {
      $form_state['conf'][$key] = $form_state['values'][$key];
    }
  }
}

/**
 * Callback for 'render callback'.
 */
function pgn_block_custom_pane_render($subtype, $conf, $args, $contexts) {
  $block = new stdClass();
  $block->title = '';

  $options = pgn_block_options_text();
  $block->content = theme('pgn_block_text', array('tag' => $conf['tag'], 'text' => $options[$conf['text']]));
  return $block;
}

/**
 * Callback for 'admin info'.
 */
function pgn_block_custom_pane_admin_info($subtype, $conf, $contexts) {
  if (!empty($conf)) {
    $block = new stdClass();
    $block->title = 'Titel und Slogan';
    $block->content = 'Pane für Titel- und Slogan-Texte in großer Schrift.';
    return $block;
  }
}
